define(["app"], function (app) {
  "use strict";

  app.controller("DataController", function ($uibModalInstance, data) {
    var $ctrl = this;
    $ctrl.data = data;

    $ctrl.models = {
      types: [
        { extension: "html", label: "Markup", data: $ctrl.data[0] },
        { extension: "json", label: "Json", data: $ctrl.data[1] },
      ],
      activeTab: "html",
    };

    $ctrl.cancel = function () {
      $uibModalInstance.dismiss("cancel");
    };
  });
});
