define(["app", "nested/data"], function (app) {
  app.controller(
    "DragDropController",
    function ($scope, $http, $q, $uibModal, $routeParams, $log) {
      $scope.page = {};
      $scope.page.pageId = $routeParams.pageId;
      //$scope.page.pageId = "1509bf8f-9a57-4ce9-97d0-8baede5a06ba";
      //$scope.page.pageId = "5091b889-99da-44fe-85e8-186855e4587e";
      $scope.open = function (size, parentSelector) {
        var parentElem = parentSelector
          ? angular.element(
              $document[0].querySelector(".modal-demo " + parentSelector)
            )
          : undefined;
        var modalInstance = $uibModal.open({
          animation: true,
          ariaLabelledBy: "modal-title",
          ariaDescribedBy: "modal-body",
          templateUrl: "app/nested/data.html",
          controller: "DataController",
          controllerAs: "$ctrl",
          size: size,
          appendTo: parentElem,
          resolve: {
            data: function () {
              return [$scope.htmlContent, $scope.modelDropzoneAsJson];
            },
          },
        });

        modalInstance.result.then(
          function () {},
          function () {
            $log.info("Modal dismissed at: " + new Date());
          }
        );
      };

      $scope.TYPE_PARENT = "tag";
      $scope.TYPE_CHILD = "module";
      $scope.UNLOCATE_PANEL = "unlocatePanel";

      $scope.models = {
        selected: null,
        templates: [
          {
            type: $scope.TYPE_PARENT,
            name: "div",
            id: "",
            class: "",
            columns: [[]],
          },
          { type: $scope.TYPE_CHILD, name: $scope.TYPE_CHILD, position: "" },
        ],
        dropzones: {
          elements: [],
        },
        modules: {
          elements: [],
        },
        originModules: {
          elements: [],
        },
      };

      $scope.dropCallback = function () {
        return false;
      };

      $scope.$watch(
        "models.dropzones",
        function (newValue, oldValue) {
          var dropzoneElements = $scope.models.dropzones.elements;
          //#region Generate tag html
          $scope.htmlContent = "";
          for (let i = 0; i < dropzoneElements.length; i++) {
            const element = dropzoneElements[i];
            if (element.type === $scope.TYPE_CHILD) {
              dropzoneElements.splice(i, 1);
              i = -1;
              if (element.position !== "" && element.position !== undefined) {
                element.hasLoadToLayout = false;
                element.position = $scope.UNLOCATE_PANEL;
                $scope.models.modules.elements.push(element);
              }
              continue;
            }
            const tag = document.createElement(`${element.name}`);
            if (element.id !== "" && element.id !== undefined) {
              tag.id = element.id;
            }
            if (element.class !== "" && element.class !== undefined) {
              tag.className = element.class;
            }
            recursiveGenerateTagHtml(element, tag);
            $scope.htmlContent += tag.outerHTML;
            $scope.htmlContent += "\n";
          }
          $scope.htmlContent = $scope.htmlContent.replace(/\n\n/gm, "\n");
          //#endregion

          //#region Assign id to position of module
          for (let j = 0; j < dropzoneElements.length; j++) {
            const element = dropzoneElements[j];
            if (element.type === $scope.TYPE_PARENT) {
              recursiveAssignPositionModule(element);
            }
          }
          //#endregion

          $scope.modelDropzoneAsJson = angular.toJson(newValue, true);
        },
        true
      );

      function recursiveAssignPositionModule(array) {
        for (let j = 0; j < array.columns.length; j++) {
          const arraySubColumns = array.columns[j];
          if (arraySubColumns !== undefined && arraySubColumns.length > 0) {
            for (let k = 0; k < arraySubColumns.length; k++) {
              const element = arraySubColumns[k];
              if (element.type === $scope.TYPE_CHILD) {
                if (array.id === "" || array.id === undefined) {
                  refreshLayout(true);
                  loadModulesToLayout();
                  removeModulesHasLoadToLayout();
                }
                if (
                  array.id !== "" &&
                  array.id !== undefined &&
                  element.position !== array.id
                ) {
                  element.position = array.id;
                  element.hasLoadToLayout = true;
                }
              } else {
                recursiveAssignPositionModule(element);
              }
            }
          }
        }
      }

      function recursiveGenerateTagHtml(array, tag) {
        for (let j = 0; j < array.columns.length; j++) {
          const arraySubColumns = array.columns[j];
          if (arraySubColumns !== undefined && arraySubColumns.length > 0) {
            for (let k = 0; k < arraySubColumns.length; k++) {
              const element = arraySubColumns[k];
              if (
                element.type === $scope.TYPE_PARENT &&
                tag !== undefined &&
                tag != null
              ) {
                const subTag = document.createElement(`${element.name}`);
                if (element.id !== "" && element.id !== undefined) {
                  subTag.id = element.id;
                }
                if (element.class !== "" && element.class !== undefined) {
                  subTag.className = element.class;
                }
                if (tag) {
                }
                tag.appendChild(document.createTextNode("\n"));
                tag.appendChild(subTag);
                tag.appendChild(document.createTextNode("\n"));
                recursiveGenerateTagHtml(element, subTag);
              }
              if (
                element.type === $scope.TYPE_CHILD &&
                (array.id === "" || array.id === undefined)
              ) {
                if (element.position === "" || element.position === undefined) {
                  const index = arraySubColumns.indexOf(element);
                  arraySubColumns.splice(index, 1);
                  k = -1;
                }
              }
            }
          }
        }
      }

      $scope.$watch(
        "models.modules",
        function (model) {
          for (let j = 0; j < $scope.models.modules.elements.length; j++) {
            const module = $scope.models.modules.elements[j];
            for (
              let i = 0;
              i < $scope.models.originModules.elements.length;
              i++
            ) {
              const originModule = $scope.models.originModules.elements[i];
              if (
                module.position === originModule.position &&
                module.hasLoadToLayout === true
              ) {
                module.hasLoadToLayout = false;
                module.position = $scope.UNLOCATE_PANEL;
              }
            }
          }
          $scope.modelModuleAsJson = angular.toJson(model, true);
        },
        true
      );

      $scope.changeIdInput = function changeIdInput() {
        refreshLayout(true);
        loadModulesToLayout();
        removeModulesHasLoadToLayout();
      };

      var refreshLayout = function (isPushBackToModule) {
        var dropzoneElements = $scope.models.dropzones.elements;
        for (let i = 0; i < dropzoneElements.length; i++) {
          const element = dropzoneElements[i];
          if (element.type === $scope.TYPE_PARENT) {
            recursiveRefreshLayout(element, isPushBackToModule);
          }
        }
      };

      function recursiveRefreshLayout(array, isPushBackToModule = false) {
        for (let j = 0; j < array.columns.length; j++) {
          const arraySubColumns = array.columns[j];
          if (arraySubColumns !== undefined && arraySubColumns.length > 0) {
            for (let k = 0; k < arraySubColumns.length; k++) {
              const element = arraySubColumns[k];
              if (element.type === $scope.TYPE_CHILD) {
                if (isPushBackToModule) {
                  element.hasLoadToLayout = false;
                  $scope.models.modules.elements.push(element);
                }
                const index = arraySubColumns.indexOf(element);
                arraySubColumns.splice(index, 1);
                k = -1;
              } else {
                recursiveRefreshLayout(element, isPushBackToModule);
              }
            }
          }
        }
      }

      var loadModulesToLayout = function () {
        for (let j = 0; j < $scope.models.modules.elements.length; j++) {
          const module = $scope.models.modules.elements[j];
          for (let i = 0; i < $scope.models.dropzones.elements.length; i++) {
            const tag = $scope.models.dropzones.elements[i];
            recursiveLoadModulesToLayout(tag, module);
          }
        }
      };

      function recursiveLoadModulesToLayout(array, module) {
        for (let j = 0; j < array.columns.length; j++) {
          var arraySubColumns = array.columns[j];
          if (arraySubColumns !== undefined && arraySubColumns.length > 0) {
            for (let k = 0; k < arraySubColumns.length; k++) {
              var element = arraySubColumns[k];
              if (
                element.type === $scope.TYPE_PARENT &&
                module !== undefined &&
                module !== null
              ) {
                var firstArray = element.columns[0];
                const indexModule = firstArray.indexOf(module);
                if (
                  module.position === element.id &&
                  element.id !== $scope.UNLOCATE_PANEL &&
                  indexModule === -1 &&
                  module.hasLoadToLayout === false
                ) {
                  firstArray.push(module);
                  module.hasLoadToLayout = true;
                }
                recursiveLoadModulesToLayout(element, module);
              }
            }
          }
        }
      }

      function removeModulesHasLoadToLayout() {
        for (let i = 0; i < $scope.models.modules.elements.length; i++) {
          const module = $scope.models.modules.elements[i];
          if (module.hasLoadToLayout) {
            $scope.models.modules.elements.splice(i, 1);
            i = -1;
          }
        }
      }

      //   $scope.changeColor = function (item) {
      //     const ul = document.getElementsByTagName("li");
      //     var listUl = Array.prototype.slice.call(ul);
      //     for (let i = 0; i < listUl.length; i++) {
      //       const element = listUl[i];
      //       const isSelected = element.className.includes("selected");
      //       if (isSelected) {
      //         element.className = element.className.replace("selected", "");
      //         element.className = element.className.replace(" ", "");
      //         break;
      //       }
      //     }
      //   };

      //#region Initialize app

      var getPageContent = function (pageId) {
        var promise = $http({
          method: "GET",
          url: "http://192.168.0.34:9900/api/pages/" + pageId,
          headers: {
            "Content-type": " application/json",
          },
        });

        promise.success(function (response) {
          $scope.page = response.Data;
        });

        return promise;
      };

      var convertLayoutContentToJson = function () {
        var array = $scope.page.Layout.LayoutContent.split("\n");
        var temp = {};
        var tempArray = [];

        for (let i = 0; i < array.length; i++) {
          const value = array[i].trim();
          if (value.startsWith("<") && !value.startsWith("<!--")) {
            var data = {};
            data.type = $scope.TYPE_PARENT;
            if (value.startsWith("</")) {
              tempArray.splice(tempArray.length - 1, 1);
              temp = tempArray[tempArray.length - 1]
                ? tempArray[tempArray.length - 1]
                : {};
            } else {
              const matchType = value.match(/<[a-zA-Z]+/);
              const matchId = value.match(/id="[^"]*"/);
              const matchClass = value.match(/class="[^"]*"/);

              data.name = matchType ? matchType[0].replace("<", "") : "";
              data.id = matchId
                ? matchId[0].replace(/\"/g, "").replace("id=", "")
                : "";
              data.class = matchClass
                ? matchClass[0].replace(/\"/g, "").replace("class=", "")
                : "";
              data.columns = [[]];

              if (Object.keys(temp).length !== 0) {
                temp.columns[0].push(data);
              } else {
                $scope.models.dropzones.elements.push(data);
              }

              if (value.includes("</") === false) {
                tempArray.push(data);
                temp = tempArray[tempArray.length - 1];
              }
            }
          }
        }
      };

      var initModules = function () {
        for (let j = 0; j < $scope.page.ListPageModule.length; j++) {
          const pageModule = $scope.page.ListPageModule[j];
          var module = {};
          module.type = $scope.TYPE_CHILD;
          module.name = pageModule.ModuleName;
          module.position = pageModule.Position;
          module.hasLoadToLayout = false;
          $scope.models.modules.elements.push(module);
          $scope.models.originModules.elements.push(module);
        }
      };

      function Init() {
        var pagePromise = getPageContent($scope.page.pageId);
        $q.all([pagePromise]).then(function (result) {
          convertLayoutContentToJson();
          initModules();
          loadModulesToLayout();
          removeModulesHasLoadToLayout();
        });
      }

      Init();
      //#endregion End initialize
    }
  );
});
