define(function() {
    'use strict';
    
    var factory = {};
    factory.setupRouteConfig = function (app, angularAMD){
        app.config(function($routeProvider) {
            $routeProvider
                .when('/drag-drop/:pageId', angularAMD.route({
                    templateUrl: 'app/nested/nested-frame.html',
                    controller: 'DragDropController',
                    controllerUrl: 'nested/nested',
                    reloadOnSearch: false
                }))
                .otherwise({redirectTo: '/drag-drop/1509bf8f-9a57-4ce9-97d0-8baede5a06ba'});
        })
    }
    return factory;
});