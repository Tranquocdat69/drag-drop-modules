define([
  "angular-amd",
  "custom-route",
  "angular-route",
  "angular-animate",
  "angular-sanitize",
  "ui-bootstrap",
  "drag-drop-list",
], function (angularAMD, customRoute) {
  "use strict";

  var app = angular.module("demo", [
    "ngRoute",
    "ngAnimate",
    "ngSanitize",
    "ui.bootstrap",
    "dndLists",
  ]);

  app.run([
    "$http",
    function ($http) {
      $http.defaults.headers.common["X-User"] =
        "8084bd74-9e1f-48f8-b957-7c69611fa046";
      $http.defaults.headers.common["X-ApplicationId"] =
        "06baa6e3-e010-4307-b702-2740066aeeca";
    },
  ]);

  customRoute.setupRouteConfig(app, angularAMD);

  return angularAMD.bootstrap(app);
});
