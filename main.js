require.config({
    baseUrl: "/app",
    paths:{
        "angular": "framework/vendor/angular.min",
        "angular-route": "framework/vendor/angular-route.min",
        "angular-animate": "framework/vendor/angular-animate.min",
        "angular-sanitize": "framework/vendor/angular-sanitize.min",
        "angular-amd": "framework/vendor/angularAMD.min",
        "ui-bootstrap": "framework/vendor/ui-bootstrap-tpls",
        "drag-drop-list": "framework/vendor/angular-drag-and-drop-lists",
        "custom-route": "framework/route"
    },
    shim:{
        'angular-route': ['angular'],
        'angular-animate': ['angular'],
        'angular-sanitize': ['angular'],
        'angular-amd': ['angular'],
        'ui-bootstrap': ['angular'],
        'drag-drop-list': ['angular']
    },
    deps: ['app']
});